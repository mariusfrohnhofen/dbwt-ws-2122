<?php
/**
 * Praktikum DBWT. Autoren:
 * John Robin, Nolan, 3220391
 * Marius, Frohnhofen, 3215267
 */

//Übergebene Parameter
const GET_PARAM_SEARCH_TEXT = 'search_text';
const GET_PARAM_SORTING = 'sorting';

$searchTerm = "";
//0 sorting nach Name, 1 sorting nach Email
$sorting = "0";
if (!empty($_GET[GET_PARAM_SEARCH_TEXT])) {
    $searchTerm = $_GET[GET_PARAM_SEARCH_TEXT];
}
if (!empty($_GET[GET_PARAM_SORTING])) {
    $sorting = $_GET[GET_PARAM_SORTING];
}
$searchTerm = strtolower($searchTerm);

$data = [];
$file="newsletter_data.txt";
$handle = fopen($file, "r");
while(!feof($handle)){
    $line = fgets($handle);
    $line_data = explode(',', $line);
    $tmp = [
        'name' => $line_data[0] ?? null,
        'email' => $line_data[1] ?? null,
        'sprache' => $line_data[2] ?? null
    ];

    array_push($data, $tmp);
}

//In Reults werden alle Suchergebnisse gespeichert
$results = [];

unset($data[count($data)-1]);
fclose($handle);

function sortEmail($a, $b) {
    return strcmp(strtolower($a["email"]), strtolower($b["email"]));
}
function sortName($a, $b) {
    return strcmp(strtolower($a["name"]), strtolower($b["name"]));
}
foreach ($data as $d) {
    if (strpos(strtolower($d['name']), $searchTerm) !== False) {
        array_push($results, $d);
    }
}

$data = $results;

//Nach welcher Methode wird sortiert
if ($sorting === "0") {
    usort($data, "sortName");
}
if ($sorting === "1") {
    usort($data, "sortEmail");
}

?>

<!DOCTYPE html>
<!--
    Praktikum DBWT. Autoren:
    Marius, Frohnhofen, 3215267
    John Robin, Nolan, 3220391
-->


<html lang="de">
<head>
    <meta charset="utf-8"/>
    <title>E-Mensa</title>
    <style>
        .tabelle {
            width: 50%;
        }
        tr,td,th {
            border: 1px solid black;
        }
    </style>
</head>
<body>
<div class="grid-container">
    <div>
        <h2>Newsletter Anmeldungen</h2>
        <form method="get">
            <label for="search_text">Filter:</label>
            <input id="search_text" type="text" name="search_text" value="">
            <br>
            <label for="sort">Sortieren nach:</label><br>
            <select id="sort" name="sorting">
                <option value="0">Name</option>
                <option value="1">Email</option>
            </select>
            <br>
            <input type="submit" value="Suchen">
        </form>
        <table class="tabelle">
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Email
                </th>
                <th>
                    Sprache
                </th>
                <th>
                    Datenschutz?
                </th>
            </tr>
            <?php
            foreach ($data as $d) {
                echo "
                        <tr>
                            <td>{$d['name']}</td>
                            <td>{$d['email']}</td>
                            <td>{$d['sprache']}</td>
                            <td>Ja</td>
                        </tr>
                    ";
            }
            ?>
        </table>
    </div>
</div>
</body>
</html>
