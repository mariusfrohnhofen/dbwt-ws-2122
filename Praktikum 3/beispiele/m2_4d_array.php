<!Doctype html>
<!--
    Praktikum DBWT. Autoren:
    Marius, Frohnhofen, 3215267
    John Robin, Nolan, 3220391
-->

<html>
<head>
	<title>Array</title>
</head>
<body>
<?php

$famousMeals = [
 1 => ['name' => 'Currywurst mit Pommes',
 'winner' => [2001, 2003, 2007, 2010, 2020]],
 2 => ['name' => 'Hähnchencrossies mit Paprikareis',
 'winner' => [2002, 2004, 2008]],
 3 => ['name' => 'Spaghetti Bolognese',
 'winner' => [2011, 2012, 2017]],
 4 => ['name' => 'Jägerschnitzel mit Pommes',
 'winner' => 2019]
];

echo '<ol>';
foreach ($famousMeals as $element) {
	$winners = $element['winner'];
	echo '<li>'.'<p>'.$element['name'].'<br>';
	$wtype = gettype($winners);
	if ($wtype == "array") { 
		for ($i = count($winners) - 1; $i >= 0; $i--) {
			echo $winners[$i];
			if ($i != 0) {
        			echo ", ";
        		}
		}
	}
	else {
    		echo $winners;
    	}
	echo '<p>'.'</li>';
}
echo '</ol>';

function gaps($arr) {
	$resArr = [];
	for ($i = 2000; $i <= 2021; $i++) {
    	$missing = true;
        foreach($arr as $element) {
        	$winners = $element['winner'];
            $wtype = gettype($winners);
            if ($wtype == "array") {
            	foreach($winners as $year) {
                	if ($i == $year) {
                		$missing = false;
                	}
                } 
            }
            else {
            	if ($i == $winners) {
                	$missing = false;
                }
            }
        }
        if($missing) {
        	array_push($resArr, $i);
        }
    }
    return $resArr;
}

$result = gaps($famousMeals);
echo "Jahre ohne Gewinner:";
echo '<ul>';
	foreach($result as $y) {
    	echo '<li>'.$y.'</li>';
    }
echo '</ul>';

?>
</body>
</html>