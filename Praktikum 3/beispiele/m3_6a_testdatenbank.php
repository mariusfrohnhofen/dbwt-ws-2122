<?php
/**
 * Praktikum DBWT. Autoren:
 * John Robin, Nolan, 3220391
 * Marius, Frohnhofen, 3215267
 */

$link=mysqli_connect("127.0.0.1", // Host der Datenbank
    "root",                 // Benutzername zur Anmeldung
    "passpass",    // Passwort
    "emensawerbeseite"      // Auswahl der Datenbanken (bzw. des Schemas)
// optional port der Datenbank
);

if (!$link) {
    echo "Verbindung fehlgeschlagen: ", mysqli_connect_error();
    exit();
}

$sql = "SELECT erfasst_am, name FROM gericht ORDER BY name";

$result = mysqli_query($link, $sql);
if (!$result) {
    echo "Fehler während der Abfrage:  ", mysqli_error($link);
    exit();
}

$data = [];

while ($row = mysqli_fetch_assoc($result)) {
    array_push($data, $row);
}

mysqli_free_result($result);
mysqli_close($link);
?>

<!DOCTYPE html>
<!--
    Praktikum DBWT. Autoren:
    Marius, Frohnhofen, 3215267
    John Robin, Nolan, 3220391
-->


<html lang="de">
<head>
    <meta charset="utf-8"/>
    <title>E-Mensa</title>
    <style>
        .tabelle {
            width: 50%;
        }
        tr,td,th {
            border: 1px solid black;
        }
    </style>
</head>
<body>
<div class="grid-container">
    <div>
        <h2>Testdatenbank</h2>
        <table class="tabelle">
            <tr>
                <th>
                    Erfasst am
                </th>
                <th>
                    Name
                </th>
            </tr>
            <?php
            foreach ($data as $d) {
                echo "
                        <tr>
                            <td>{$d['erfasst_am']}</td>
                            <td>{$d['name']}</td>
                        </tr>
                    ";
            }
            ?>
        </table>
    </div>
</div>
</body>
</html>

