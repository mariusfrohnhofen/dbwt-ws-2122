<?php
/**
 * Praktikum DBWT. Autoren:
 * John Robin, Nolan, 3220391
 * Marius, Frohnhofen, 3215267
 */

$filename = 'log.txt';

$date_and_time = date("d/m/Y h:i:s");
$browser = $_SERVER['HTTP_USER_AGENT'];
$ip = $_SERVER['REMOTE_ADDR'];
$new_line = $date_and_time." | ".$browser." | ".$ip;

$new_line = $new_line . "\n";

file_put_contents($filename, $new_line, FILE_APPEND);

?>