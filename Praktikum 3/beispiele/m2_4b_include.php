<?php
/**
 * Praktikum DBWT. Autoren:
 * John Robin, Nolan, 3220391
 * Marius, Frohnhofen, 3215267
 */

include 'm2_4a_standardparameter.php';

$bsp11 = 2;
$bsp12 = 5;
echo addieren($bsp11, $bsp12);

$bsp2 = 3;
echo addieren($bsp2);

$bsp31 = 9;
$bsp32 = 7;
$bsp33 = addieren($bsp31, $bsp32);
echo addieren($bsp33, $bsp31);