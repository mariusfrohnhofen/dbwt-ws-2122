<?php
/**
 * Praktikum DBWT. Autoren:
 * John Robin, Nolan, 3220391
 * Marius, Frohnhofen, 3215267
 */

include 'meals.php';

const GET_PARAM_NEWSLETTER_NAME = 'newsletter_name';
const GET_PARAM_NEWSLETTER_EMAIL = 'newsletter_email';
const GET_PARAM_NEWSLETTER_LANGUAGE = 'newsletter_language';

$trash_mails = [
    'rcpt.at',
    'damnthespam.at',
    'wegwerfmail.de',
    'trashmail.'
];


$newsletter_name = '';
$newsletter_email = '';
$newsletter_language = '';
$show_message = false;
$error_message = '';

if (!empty($_POST[GET_PARAM_NEWSLETTER_LANGUAGE])) {
    $newsletter_language = $_POST[GET_PARAM_NEWSLETTER_LANGUAGE];
}

if (!empty($_POST[GET_PARAM_NEWSLETTER_NAME])) {
    $newsletter_name = $_POST[GET_PARAM_NEWSLETTER_NAME];

    if ($newsletter_name == ' ') {
        $error_message = 'Geben Sie einen gültigen Namen an.';
        $show_message = true;
    }
    else {
        $show_message = true;
    }
}
if (!empty($_POST[GET_PARAM_NEWSLETTER_EMAIL])) {
    $newsletter_email = $_POST[GET_PARAM_NEWSLETTER_EMAIL];

    foreach ($trash_mails as $trash_mail) {
        if (stripos($newsletter_email, $trash_mail) !== false) {
            $error_message = 'Geben Sie keine Trashmail an.';
            $show_message = true;
        }
    }
}
else {
    $show_message = false;
}

if ($show_message) {
    if ($error_message == '') {
        $data = $newsletter_name . ", " . $newsletter_email . ", " . $newsletter_language . "\n";

        file_put_contents(
             'newsletter_data.txt',
            $data,
            FILE_APPEND
        );

        echo "<script type='text/javascript'>alert('Sie wurden zum Newsletter angemeldet');</script>";
    }
    else {
        echo "<script type='text/javascript'>alert('$error_message');</script>";
    }
}


?>


<!DOCTYPE html>
<!--
    Praktikum DBWT. Autoren:
    Marius, Frohnhofen, 3215267
    John Robin, Nolan, 3220391
-->


<html lang="de">
<head>
    <meta charset="utf-8"/>
    <title>E-Mensa</title>
    <style>
        body {
            font-family: Arial;
        }

        .grid-container {
            display: grid;
            grid-template-columns: 20% 60% 20%;
            grid-template-rows: auto 80px auto auto auto auto auto auto auto;
        }

        .grid-container > div {
            background-color: rgba(255, 255, 255, 1);
            padding: 20px 0;
            font-size: 15px;
            text-align: center;
        }

        .tabelle {
            width: 100%;
        }
        .tabelle td {
            border: 1px solid black;
        }

        div > h2 {
            text-align: left;
        }

        #wir-freuen-uns > h2{
            text-align: center;
        }

        .wichtig-bullets {
            text-align: left;
        }
        #newsletterTabelle {
            text-align: left;
        }
        #zahlenTabelle {
            width: 100%;
        }

        .strichOben {
            border-top: 1px solid black;
        }

        #footerTabelle {
            width: 100%;
        }

        #verlinkungenTabelle {
            width: 100%;

        }

        .kleinerTeil {
            width: 13%;
        }
        .grosserTeil {
            width: 20%;
        }
        .gericht_foto {
            width: 30%;
        }

    </style>
</head>
<body>
<div class="grid-container">
    <div id="logo">
        <h2>E-Mensa Logo</h2>
    </div>
    <div id="verlinkungen">
        <table id="verlinkungenTabelle">
            <tr>
                <td>
                    <a href="#ankuendigung">Ankündigung</a>
                </td>
                <td>
                    <a href="#speisen">Speisen</a>
                </td>
                <td>
                    <a href="#zahlen">Zahlen</a>
                </td>
                <td>
                    <a href="#kontakt">Kontakt</a>
                </td>
                <td>
                    <a href="#wichtig-fuer-uns">Wichtig für uns</a>
                </td>
            </tr>
        </table>
    </div>
    <div class="leer">

    </div>
    <div class="leer strichOben">

    </div>
    <div id="platzhalter" class="strichOben">
    </div>
    <div class="leer strichOben">

    </div>
    <div class="leer">

    </div>
    <div id="ankuendigung">
        <h2>Bald gibt es Essen auch online :)</h2>
        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
    </div>
    <div class="leer">

    </div>
    <div class="leer">

    </div>
    <div id="speisen">
        <h2>Köstlichkeiten, die Sie erwarten</h2>
        <table class="tabelle">
            <tr>
                <td style="width:50%;"></td>
                <td></td>
                <td>Preis intern</td>
                <td>Preis extern</td>
            </tr>
            <?php
                foreach ($meals as $meal) {
                    echo "
                        <tr>
                            <td><img class='gericht_foto' src='{$meal['img_src']}'></td>
                            <td>{$meal['description']}</td>
                            <td>{$meal['price_intern']}</td>
                            <td>{$meal['price_extern']}</td>
                        </tr>
                    ";
                }
            ?>
        </table>
    </div>
    <div class="leer">

    </div>
    <div class="leer">

    </div>
    <div id="zahlen">
        <h2>E-Mensa in Zahlen</h2>
        <table id="zahlenTabelle">
            <tr>
                <td class="kleinerTeil">
                    <h3>420</h3>
                </td>
                <td class="grosserTeil">
                    Besuche
                </td>
                <td class="kleinerTeil">
                    <h3>28</h3>
                </td>
                <td class="grosserTeil">
                    Anmeldungen zum Newsletter
                </td>
                <td class="kleinerTeil">
                    <h3>4</h3>
                </td>
                <td class="grosserTeil">
                    Speisen
                </td>
            </tr>
        </table>
    </div>
    <div class="leer">

    </div>
    <div class="leer">

    </div>
    <div id="kontakt">
        <h2>Interesse geweckt? Wir infomieren Sie!</h2>
        <form action="index.php" method="post">
            <table id="newsletterTabelle">
                <tr>
                    <td>
                        <label for="name">Ihr Name:</label><br>
                        <input type="text" id="name" name="newsletter_name" required>
                    </td>
                    <td>
                        <label for="email">Ihre E-Mail:</label><br>
                        <input type="email" id="email" name="newsletter_email" required>
                    </td>
                    <td>
                        <label for="sprache">Newsletter bitte in:</label><br>
                        <select id="sprache" name="newsletter_language">
                            <option value="deutsch">Deutsch</option>
                            <option value="englisch">Englisch</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <input type="checkbox" id="datenschutz" name="datenschutz" required>
                        <label for="datenschutz">Den Datenschutzbestimmungen stimme ich zu</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <button >Zum Newsletter anmelden</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div class="leer">

    </div>
    <div class="leer">

    </div>
    <div id="wichtig-fuer-uns">
        <h2>Das ist uns wichtig</h2>
        <ul class="wichtig-bullets">
            <li>Beste frische saisonale Zutaten</li>
            <li>Ausgewogene abwechslungsreiche Gerichte</li>
            <li>Sauberkeit</li>
        </ul>
    </div>
    <div class="leer">

    </div>
    <div class="leer">

    </div>
    <div id="wir-freuen-uns">
        <h2>Wir freuen uns auf Ihren Besuch!</h2>
    </div>
    <div class="leer">

    </div>
    <div class="leer strichOben">

    </div>
    <div id="footer" class="strichOben">
        <table id="footerTabelle">
            <tr>
                <td>
                    © E-Mensa GmbH
                </td>
                <td>
                    Marius Frohnhofen, 3215267<br>
                    John Robin Nolan, 3220391
                </td>
                <td>
                    <a href="">Impressum</a>
                </td>
            </tr>
        </table>
    </div>
    <div class="leer strichOben">

    </div>
</div>
</body>
</html>