<?php

$meals = [
    [
        'description' => 'Rindfleisch mit Bambus, Kaiserschoten und rotem<br>Paprika, dazu Mie Nudeln',
        'price_intern' => '3,50€',
        'price_extern' => '5,30€',
        'img_src' => 'img/rindfleisch.jpg'
    ],
    [
        'description' => 'Spinatrisotto mit kleinen Saosateigecken<br>und gemischter Salat',
        'price_intern' => '2,90€',
        'price_extern' => '5,30€',
        'img_src' => 'img/risotto.jpg'
    ],
    [
        'description' => 'Spaghetti Bolognese<br>mit frischem Parmesan',
        'price_intern' => '3,00€',
        'price_extern' => '4,50€',
        'img_src' => 'img/spaghetti.jpg'
    ],
    [
        'description' => 'Chili sin carne (vegan)',
        'price_intern' => '2,10€',
        'price_extern' => '3,70€',
        'img_src' => 'img/chili.jpg'
    ]
];

?>