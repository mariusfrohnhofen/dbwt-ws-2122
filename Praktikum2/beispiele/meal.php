<?php
/**
* Praktikum DBWT. Autoren:
* John Robin, Nolan, 3220391
* Marius, Frohnhofen, 3215267
*/

const GET_PARAM_MIN_STARS = 'search_min_stars';
const GET_PARAM_SEARCH_TEXT = 'search_text';
const GET_PARAM_SHOW_DESCRIPTION = 'show_description';
const GET_PARAM_LANGUAGE = 'sprache';

/**
 * List of all allergens.
 */
$words_de = [
    'gericht' => 'Gericht',
    'allergene' => 'Allergene',
    'bewertungen_insgesamt' => 'Bewertungen (Insgesamt: ',
    'suchen' => 'Suchen',
    'sterne' => 'Sterne',
    'autor' => 'Autor',
    'preis_intern' => 'Preis intern: ',
    'preis_extern' => 'Preis extern: '
];

$words_en = [
    'gericht' => 'Dish',
    'allergene' => 'Allergens',
    'bewertungen_insgesamt' => 'Rating (All: ',
    'suchen' => 'Search',
    'sterne' => 'Stars',
    'autor' => 'Author',
    'preis_intern' => 'Price intern: ',
    'preis_extern' => 'Price extern: '
];

$allergens_de = [
    11 => 'Gluten',
    12 => 'Krebstiere',
    13 => 'Eier',
    14 => 'Fisch',
    17 => 'Milch'
];

$allergens_en = [
    11 => 'Gluten',
    12 => 'Crustaceans',
    13 => 'Eggs',
    14 => 'Fish',
    17 => 'Milk'
];

$meal_de = [
    'name' => 'Süßkartoffeltaschen mit Frischkäse und Kräutern gefüllt',
    'description' => 'Die Süßkartoffeln werden vorsichtig aufgeschnitten und der Frischkäse eingefüllt.',
    'price_intern' => 2.90,
    'price_extern' => 3.90,
    'allergens' => [11, 13],
    'amount' => 42             // Number of available meals
];

$meal_en = [
    'name' => 'Sweet potato pockets filled with cream cheese and herbs',
    'description' => 'Carefully cut open the sweet potatoes and pour in the cream cheese.',
    'price_intern' => 2.90,
    'price_extern' => 3.90,
    'allergens' => [11, 13],
    'amount' => 42             // Number of available meals
];

$ratings_de = [
    [   'text' => 'Die Kartoffel ist einfach klasse. Nur die Fischstäbchen schmecken nach Käse. ',
        'author' => 'Ute U.',
        'stars' => 2 ],
    [   'text' => 'Sehr gut. Immer wieder gerne',
        'author' => 'Gustav G.',
        'stars' => 4 ],
    [   'text' => 'Der Klassiker für den Wochenstart. Frisch wie immer',
        'author' => 'Renate R.',
        'stars' => 4 ],
    [   'text' => 'Kartoffel ist gut. Das Grüne ist mir suspekt.',
        'author' => 'Marta M.',
        'stars' => 3 ]
];


$sprache = "de";
$searchTerm = "";
$show_description = 1;
$showRatings = [];
if (!empty($_GET[GET_PARAM_SHOW_DESCRIPTION])) {
    $show_description = $_GET[GET_PARAM_SHOW_DESCRIPTION];
}
if (!empty($_GET[GET_PARAM_LANGUAGE])) {
    $sprache = $_GET[GET_PARAM_LANGUAGE];
}

if (!empty($_GET[GET_PARAM_SEARCH_TEXT])) {
    $searchTerm = $_GET[GET_PARAM_SEARCH_TEXT];
    foreach ($ratings_de as $rating) {
        if (stripos($rating['text'], $searchTerm) !== false) {
            $showRatings[] = $rating;
        }
    }
} else if (!empty($_GET[GET_PARAM_MIN_STARS])) {
    $minStars = $_GET[GET_PARAM_MIN_STARS];
    foreach ($ratings_de as $rating) {
        if ($rating['stars'] >= $minStars) {
            $showRatings[] = $rating;
        }
    }
} else {
    $showRatings = $ratings_de;
}

function calcMeanStars(array $ratings) : float {
    $sum = 0;
    foreach ($ratings as $rating) {
        $sum += $rating['stars'] / count($ratings);
    }
    return $sum;
}

?>
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="UTF-8"/>
        <title>
            <?php
                if ($sprache == 'de') {
                    echo $words_de['gericht'] . ": ";
                }
                else if ($sprache == 'en') {
                    echo $words_en['gericht'] . ": ";
                }
                if ($sprache == 'de') {
                    echo $meal_de['name'];
                }
                else if ($sprache == 'en') {
                    echo $meal_en['name'];
                }
            ?>

        </title>
        <style type="text/css">
            * {
                font-family: Arial, serif;
            }
            .rating {
                color: darkgray;
            }
        </style>
    </head>
    <body>
        <h1>
            <?php
                if ($sprache == 'de') {
                    echo $words_de['gericht'] . ": ";
                }
                else if ($sprache == 'en') {
                    echo $words_en['gericht'] . ": ";
                }
                if ($sprache == 'de') {
                    echo $meal_de['name'];
                }
                else if ($sprache == 'en') {
                    echo $meal_en['name'];
                }
            ?></h1>
        <p><?php
                if ($show_description == 1) {
                    if ($sprache == 'de') {
                        echo $meal_de['description'];
                    } else if ($sprache == 'en') {
                        echo $meal_en['description'];
                    }
                }
            ?></p>
        <p>
            <?php
                if ($sprache == 'de') {
                    echo $words_de['allergene'] . ": ";
                }
                else if ($sprache == 'en') {
                    echo $words_en['allergene'] . ": ";
                }

                if ($sprache == 'de') {
                    foreach($meal_de['allergens'] as $allergen) {
                        echo $allergens_de[$allergen] . ", ";
                    }
                } else if ($sprache == 'en') {
                    foreach($meal_en['allergens'] as $allergen) {
                        echo $allergens_en[$allergen] . ", ";
                    }
                }



            ?>
        </p>
        <p>
            <?php
                if ($sprache == 'de') {
                    echo $words_de['preis_intern'];
                }
                else if ($sprache == 'en') {
                    echo $words_en['preis_intern'];
                }
                setlocale(LC_MONETARY,"en_US");
                echo number_format((float)$meal_de['price_intern'], 2, ',', '') . "€";
            ?>
        </p>
        <p>
            <?php
                if ($sprache == 'de') {
                    echo $words_de['preis_extern'];
                }
                else if ($sprache == 'en') {
                    echo $words_en['preis_extern'];
                }
                echo number_format((float)$meal_de['price_extern'], 2, ',', '') . "€";
            ?>
        </p>
        <h1>
            <?php
                if ($sprache == 'de') {
                    echo $words_de['bewertungen_insgesamt'];
                }
                else if ($sprache == 'en') {
                    echo $words_en['bewertungen_insgesamt'];
                }
                echo calcMeanStars($ratings_de)
            ?>)
        </h1>
        <form method="get">
            <label for="search_text">Filter:</label>
            <input id="search_text" type="text" name="search_text" value=<?php echo $searchTerm;?>>
            <input type="hidden" name="sprache" value=<?php echo $sprache;?> />
            <input type="submit" value=<?php
            if ($sprache == 'de') {
                echo $words_de['suchen'];
            }
            else if ($sprache == 'en') {
                echo $words_en['suchen'];
            }
            ?>>
        </form>
        <table class="rating">
            <thead>
            <tr>
                <td>
                    Text
                </td>
                <td>
                    <?php
                        if ($sprache == 'de') {
                            echo $words_de['sterne'];
                        }
                        else if ($sprache == 'en') {
                            echo $words_en['sterne'];
                        }
                    ?>
                </td>
                <td>
                    <?php
                        if ($sprache == 'de') {
                            echo $words_de['autor'];
                        }
                        else if ($sprache == 'en') {
                            echo $words_en['autor'];
                        }
                    ?>
                </td>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($showRatings as $rating) {
                    echo "<tr><td class='rating_text'>{$rating['text']}</td>
                      <td class='rating_stars'>{$rating['stars']}</td>
                      <td class='rating_author'>{$rating['author']}</td>
                  </tr>";
                }?>
            <a href="?sprache=de">Deutsch</a>
            <a href="?sprache=en">English</a>
            </tbody>
        </table>
    </body>
</html>